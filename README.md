If you see this, you're in the right spot :)
# RRHS-RAP-Bulletin

Welcome to the bulletin for Research and Presentation Club at Round Rock High School!
Here you'll find everything you need to know about our club. Included in the files above are files pertaining to how we will operate along with many delightful resources which we will add as our club progresses!

### Meeting Information
Our next meeting is on **November 16th**, 2023, **Thursday**, at room **2518** in 
Mr. Rolofson's room.
The schedule for the day is as follows:
|        Topic        |   Time   |      Presenter       |
| ------------------- | -------  | -------------------- |
| Scrum & Discussion  | 4:30 PM  |    N/A     |

### Important Links
[Presentaion Submission](https://forms.gle/7tGw9DHqnjHXAqbGA)

### A normal club day
Meetings will start any time at or before 4:35PM. Meetings will start with informational presentations which will be given out, (usually by officers but members are welcome to give one too), on methodology to help you in your work. Afterwards, we will have scrum, where everyone gets into a circle and share what they've been working on. (Remember that if you've made no progress on your project, don't be ashamed to say so!) Lastly, if there are any presentations for the day, they will be given then. Everything that happens in a meeting will be scheduled by the minute, so we can hold ourselves accountable from going overtime.n

### Contact
Julian Moreno s148998@student.roundrockisd.org  
Kaif Ilias s572793@student.roundrockisd.org  
Nikhil Prabhu s504396@student.roundrockisd.org  
Mr. Rolofson benjamin_rolofson@roundrockisd.org